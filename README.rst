Api Agenda
=====


Api de servicio rest de agenda, en Spring boot en mysql, contine archivo docker-compose.yml para crear un contenedor con la base de datos, permite adicionar datos personales de una persona y su nucleo familiar a una tabla de mysql

La base de datos se cargara desde docker, el directorio docker, se encuentra el archivo docker-compose.yml con la configuracion.
para iniciar el servidor mysql se debe ejecutar el comando docker-compose up  

una vez se inicia  el servidor mysql desde docker, se puede lanzar el servicio  que creara la estructura de mysql necesaria.


evidencias
----------

..
1-test junit
----------
.. image:: evidencias/1-junit.png
   :width: 400px
   :align: center
   :alt: test junit

2-get all sin datos - consulta  cuyo retorno activa NotFoundException 
----------
.. image:: evidencias/2-getall.png
   :width: 400px
   :align: center
   :alt: get all sin datos
   

3-post - crea una persona, persistiendo a su madre y padre 
----------
.. image:: evidencias/3-post.png
   :width: 400px
   :align: center
   :alt: post crea una persona
   

4-get persona por identificacion
----------
.. image:: evidencias/4-getById.png
   :width: 400px
   :align: center
   :alt: consulta una persona por su identificacion
   

5-borrar una persona por identificacion
----------
.. image:: evidencias/5-deleteById.png
   :width: 400px
   :align: center
   :alt: elimina una persona segun su identificacion
   

6-actualizar datos de una persona
----------
.. image:: evidencias/6-actualizarDatos.png
   :width: 400px
   :align: center
   :alt: actualiza informacino de una persona


7-get all con datos - consulta  datos con informacion
----------
.. image:: evidencias/7-getAllDatos.png
   :width: 400px
   :align: center
   :alt: get all con datos