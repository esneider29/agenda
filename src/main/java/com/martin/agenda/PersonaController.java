package com.martin.agenda;

import java.util.List;

import javax.websocket.server.PathParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.martin.agenda.service.IPersonaService;
import com.martin.agenda.service.dto.PersonaDto;

@RestController
@RequestMapping(path="/agenda")
public class PersonaController {
	@Autowired
	private IPersonaService personaService;	
	Logger logger = LoggerFactory.getLogger(PersonaController.class);

	
    @GetMapping       
	public List<PersonaDto> getPersonas () {        
		return personaService.getPersonas();	    
	}    
    
    @GetMapping("/persona")
    public PersonaDto getPersonaById(@PathParam("identificacion") final Integer identificacion){    	
        return personaService.getPersonaById(identificacion);
    }
         
	@PostMapping
	  public void save (@RequestBody final PersonaDto personaDto) {		
	    personaService.save(personaDto);	    
	  }
	
	@DeleteMapping
    public void delete(@PathParam("identificacion") final Integer identificacion){		
        personaService.delete(identificacion);
    }
	
	@PutMapping("/persona")
	public void update(@RequestBody final PersonaDto personaDto){			
        personaService.update(personaDto);
    }	
		

}
