package com.martin.agenda.config;

import java.util.Arrays;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DozerConfiguration {
	public DozerConfiguration(){
    }
 
    @Bean
    public Mapper buildMapper(){
        return new DozerBeanMapper(Arrays.asList("mapping.xml"));
    }

}
