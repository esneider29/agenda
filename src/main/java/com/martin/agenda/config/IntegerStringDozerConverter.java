package com.martin.agenda.config;

import org.dozer.DozerConverter;

public class IntegerStringDozerConverter extends DozerConverter<Integer, String> {
	 
    public IntegerStringDozerConverter(final Class<Integer> prototypeA, final Class<String> prototypeB) {
        super(prototypeA, prototypeB);
    }
 
    public IntegerStringDozerConverter() {
        super(Integer.class, String.class);
    }
 
    @Override
    public String convertTo(final Integer source, final String destination) {    	
            return source.toString();    	
    }

    @Override
    public Integer convertFrom(final String source, final Integer destination) {    	    	
        return Integer.valueOf(source);
    }
}