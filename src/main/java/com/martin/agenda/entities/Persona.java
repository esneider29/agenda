package com.martin.agenda.entities;


import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;


@Entity(name="PERSONA")
public class Persona {
	@Id

	private Integer identificacion;
	
	@Column(nullable = false)
	private String nombre1;	
	private String nombre2;	
	@Column(nullable = false)	
	private String apellido1;
	private String apellido2;
	
	
//relacion  muchos  a uno; muchos hijos pueden tener un mismo padre

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="padre_id")
	private Persona padre;

//uno a muchos por que muchos hijos pueden tienen 1 padre
	@OneToMany(mappedBy="padre")
	private Set<Persona> hijosPadre = new HashSet<>();

	
	//relacion  muchos  a uno; muchos hijos pueden tener una misma madre
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="madre_id")
	private Persona madre;

	//una madre puede tener muchos hijos
	@OneToMany(mappedBy="madre")
	private Set<Persona> hijosMadre = new HashSet<>();

	public Persona() {
		
	}

	public Persona(Integer identificacion, String nombre1, String apellido1) {
		this.identificacion = identificacion;
		this.nombre1 = nombre1;
		this.apellido1 = apellido1;
    }

	public Persona getPadre() {
		return padre;
	}

	public void setPadre(Persona padre) {
		this.padre = padre;
	}

	public Persona getMadre() {
		return madre;
	}

	public void setMadre(Persona madre) {
		this.madre = madre;
	}


	public Integer getIdentificacion() {
		return identificacion;
	}


	public void setIdentificacion(Integer identificacion) {
		this.identificacion = identificacion;
	}


	public String getNombre1() {
		return nombre1;
	}


	public void setNombre1(String nombre1) {
		this.nombre1 = nombre1;
	}


	public String getNombre2() {
		return nombre2;
	}


	public void setNombre2(String nombre2) {
		this.nombre2 = nombre2;
	}


	public String getApellido1() {
		return apellido1;
	}


	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}


	public String getApellido2() {
		return apellido2;
	}


	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((apellido1 == null) ? 0 : apellido1.hashCode());
		result = prime * result + ((apellido2 == null) ? 0 : apellido2.hashCode());
		result = prime * result + ((hijosMadre == null) ? 0 : hijosMadre.hashCode());
		result = prime * result + ((hijosPadre == null) ? 0 : hijosPadre.hashCode());
		result = prime * result + ((identificacion == null) ? 0 : identificacion.hashCode());
		result = prime * result + ((madre == null) ? 0 : madre.hashCode());
		result = prime * result + ((nombre1 == null) ? 0 : nombre1.hashCode());
		result = prime * result + ((nombre2 == null) ? 0 : nombre2.hashCode());
		result = prime * result + ((padre == null) ? 0 : padre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Persona))
			return false;
		Persona other = (Persona) obj;
		if (apellido1 == null) {
			if (other.apellido1 != null)
				return false;
		} else if (!apellido1.equals(other.apellido1))
			return false;
		if (apellido2 == null) {
			if (other.apellido2 != null)
				return false;
		} else if (!apellido2.equals(other.apellido2))
			return false;
		if (hijosMadre == null) {
			if (other.hijosMadre != null)
				return false;
		} else if (!hijosMadre.equals(other.hijosMadre))
			return false;
		if (hijosPadre == null) {
			if (other.hijosPadre != null)
				return false;
		} else if (!hijosPadre.equals(other.hijosPadre))
			return false;
		if (identificacion == null) {
			if (other.identificacion != null)
				return false;
		} else if (!identificacion.equals(other.identificacion))
			return false;
		if (madre == null) {
			if (other.madre != null)
				return false;
		} else if (!madre.equals(other.madre))
			return false;
		if (nombre1 == null) {
			if (other.nombre1 != null)
				return false;
		} else if (!nombre1.equals(other.nombre1))
			return false;
		if (nombre2 == null) {
			if (other.nombre2 != null)
				return false;
		} else if (!nombre2.equals(other.nombre2))
			return false;
		if (padre == null) {
			if (other.padre != null)
				return false;
		} else if (!padre.equals(other.padre))
			return false;
		return true;
	}

	
	
	
	
}
