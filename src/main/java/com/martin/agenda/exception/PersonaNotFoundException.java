package com.martin.agenda.exception;

public class PersonaNotFoundException extends RuntimeException {

	public PersonaNotFoundException(Integer identificacion){
		super("no existe identificacion " + identificacion);
	}
}
