package com.martin.agenda.repository;

import org.springframework.data.repository.CrudRepository;

import com.martin.agenda.entities.Persona;

public interface PersonaRepository extends CrudRepository<Persona, Integer>{	
    
}
