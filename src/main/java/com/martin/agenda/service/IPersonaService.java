package com.martin.agenda.service;

import java.util.List;
import com.martin.agenda.service.dto.PersonaDto;

/**
 * Api de agenda
 */
public interface IPersonaService {
	/**
     * Método que debe devolver el lista de todas las personas
     * @return
     */
    List<PersonaDto> getPersonas();
    
    /**
     * Método que debe devolver una persona a partir de su identificacion
     * @return
     * @param identificacion
     */
    PersonaDto getPersonaById(Integer identificacion);
    
    /**
     * Método que debe guardar una persona
     * @param PersonaDto
     */
    void save(PersonaDto personaDto);
      
    /**
     * Método que debe borrar una persona
     * @param identificacion
     */
    void delete(Integer identificacion);
    
    /**
     * Método que debe actualizar una persona
     * @param personaDto
     */
    void update(PersonaDto personaDto);

}