package com.martin.agenda.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.martin.agenda.entities.Persona;
import com.martin.agenda.exception.NotFoundException;
import com.martin.agenda.exception.PersonaNotFoundException;
import com.martin.agenda.repository.PersonaRepository;
import com.martin.agenda.service.dto.PersonaDto;
import org.apache.commons.lang3.Validate;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class PersonaServiceImpl implements IPersonaService {
	static final Logger logger = 
			LoggerFactory.getLogger(PersonaServiceImpl.class);
	
	@Autowired
    private PersonaRepository personaRepository;
	
	@Autowired
	private Mapper mapper;
	
	@Override
	public List<PersonaDto> getPersonas(){		

		final List<PersonaDto> personaDtoList = new ArrayList<>();
		
		Iterator<Persona> iter =  personaRepository.findAll().iterator();
				
		if (!iter.hasNext()) {
			throw new NotFoundException();	
		}
		
		personaRepository.findAll()
		   .forEach(p -> personaDtoList.add(mapper.map(p, PersonaDto.class)));		
		
		
		
		return personaDtoList;		
	}

	@Override
	public PersonaDto getPersonaById(Integer identificacion) {		
		Persona p = personaRepository.findById(identificacion).orElseThrow(() -> new PersonaNotFoundException(identificacion));		
		return mapper.map(p, PersonaDto.class);		
	}		
	
	@Override
	public void save(PersonaDto personaDto) {
		Validate.notNull(personaDto);
        personaRepository.save(mapper.map(personaDto, Persona.class));		
	}	

	@Override
	public void delete(Integer identificacion) {		
       personaRepository.deleteById(identificacion);		
	}

	@Override
	public void update(PersonaDto personaDto) {
		save(personaDto);		
	}

	
	

}
