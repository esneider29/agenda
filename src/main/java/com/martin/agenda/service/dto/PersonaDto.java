package com.martin.agenda.service.dto;

import com.martin.agenda.entities.Persona;


public class PersonaDto {		
	private Integer identificacion;	
	private String nombre1;
	private String nombre2;
	private String apellido1;	
	private String apellido2;	
	private Persona padre;
	private Persona madre;	

	public PersonaDto() {
		
	}
	public PersonaDto(Integer identificacion, String nombre1, String apellido1) {
		this.identificacion = identificacion;
		this.nombre1 = nombre1;
		this.apellido1 = apellido1;
    }
	
	/**
	 * @return the identificacion
	 */
	public Integer getIdentificacion() {
		return identificacion;
	}
	/**
	 * @param identificacion the identificacion to set
	 */
	public void setIdentificacion(Integer identificacion) {
		this.identificacion = identificacion;
	}
	/**
	 * @return the nombre1
	 */
	public String getNombre1() {
		return nombre1;
	}
	/**
	 * @param nombre1 the nombre1 to set
	 */
	public void setNombre1(String nombre1) {
		this.nombre1 = nombre1;
	}
	/**
	 * @return the nombre2
	 */
	public String getNombre2() {
		return nombre2;
	}
	/**
	 * @param nombre2 the nombre2 to set
	 */
	public void setNombre2(String nombre2) {
		this.nombre2 = nombre2;
	}
	/**
	 * @return the apellido1
	 */
	public String getApellido1() {
		return apellido1;
	}
	/**
	 * @param apellido1 the apellido1 to set
	 */
	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}
	/**
	 * @return the apellido2
	 */
	public String getApellido2() {
		return apellido2;
	}
	/**
	 * @param apellido2 the apellido2 to set
	 */
	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}
	/**
	 * @return the padre
	 */
	public Persona getPadre() {
		return padre;
	}
	/**
	 * @param padre the padre to set
	 */
	public void setPadre(Persona padre) {
		this.padre = padre;
	}
	
	/**
	 * @return the madre
	 */
	public Persona getMadre() {
		return madre;
	}
	
	/**
	 * @param madre the madre to set
	 */
	public void setMadre(Persona madre) {
		this.madre = madre;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((apellido1 == null) ? 0 : apellido1.hashCode());
		result = prime * result + ((apellido2 == null) ? 0 : apellido2.hashCode());
		result = prime * result + ((identificacion == null) ? 0 : identificacion.hashCode());
		result = prime * result + ((madre == null) ? 0 : madre.hashCode());
		result = prime * result + ((nombre1 == null) ? 0 : nombre1.hashCode());
		result = prime * result + ((nombre2 == null) ? 0 : nombre2.hashCode());
		result = prime * result + ((padre == null) ? 0 : padre.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof PersonaDto))
			return false;
		PersonaDto other = (PersonaDto) obj;
		if (apellido1 == null) {
			if (other.apellido1 != null)
				return false;
		} else if (!apellido1.equals(other.apellido1))
			return false;
		if (apellido2 == null) {
			if (other.apellido2 != null)
				return false;
		} else if (!apellido2.equals(other.apellido2))
			return false;
		if (identificacion == null) {
			if (other.identificacion != null)
				return false;
		} else if (!identificacion.equals(other.identificacion))
			return false;
		if (madre == null) {
			if (other.madre != null)
				return false;
		} else if (!madre.equals(other.madre))
			return false;
		if (nombre1 == null) {
			if (other.nombre1 != null)
				return false;
		} else if (!nombre1.equals(other.nombre1))
			return false;
		if (nombre2 == null) {
			if (other.nombre2 != null)
				return false;
		} else if (!nombre2.equals(other.nombre2))
			return false;
		if (padre == null) {
			if (other.padre != null)
				return false;
		} else if (!padre.equals(other.padre))
			return false;
		return true;
	}	
		
}
