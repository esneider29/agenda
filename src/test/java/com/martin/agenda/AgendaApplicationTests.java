package com.martin.agenda;

import static org.junit.Assert.assertEquals;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Import;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;
import com.martin.agenda.config.RestTemplateConfiguration;
import com.martin.agenda.entities.Persona;
import com.martin.agenda.repository.PersonaRepository;
import com.martin.agenda.service.dto.PersonaDto;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@Import(RestTemplateConfiguration.class)
class AgendaApplicationTests {
	static final Logger logger = 
			LoggerFactory.getLogger(AgendaApplicationTests.class);
	
	@Autowired
    private TestRestTemplate testRestTemplate;
	
	@Autowired
    private PersonaRepository personaRepository;	
	
    @BeforeEach
    public void initialization(){
    	personaRepository.deleteAll();
    }        
     
    @Test
    public void insertarUnaPersona() throws Exception {
    	final Persona padre = new Persona(888,"Francisco", "martin");
    	final Persona madre = new Persona(777,"Hilma", "romero");
    	
    	final PersonaDto personaDto = new PersonaDto(997,"Esneider", "martin");
    	personaDto.setApellido2("romero");
    	personaDto.setPadre(padre);
    	personaDto.setMadre(madre);
    	
        guardarPersonaDto(personaDto);
        
        final PersonaDto personaInsertadaDto = getPersona(personaDto.getIdentificacion());
        
        assertEquals(personaDto.getIdentificacion(), personaInsertadaDto.getIdentificacion());
        assertEquals(personaDto.getNombre1(), personaInsertadaDto.getNombre1());    	
        assertEquals(personaDto.getApellido1(), personaInsertadaDto.getApellido1());
        assertEquals(personaDto.getApellido2(), personaInsertadaDto.getApellido2());
        
        assertEquals(personaDto.getPadre().getIdentificacion(), personaInsertadaDto.getPadre().getIdentificacion());       
        assertEquals("Francisco", personaInsertadaDto.getPadre().getNombre1());
        assertEquals("martin", personaInsertadaDto.getPadre().getApellido1());
        
        assertEquals(personaDto.getPadre(), personaInsertadaDto.getPadre());
        assertEquals(personaDto.getMadre(), personaInsertadaDto.getMadre());
    }
    
    @Test
    public void devuelveNuloDespuesDeInsertarYBorrar() throws Exception {
    	PersonaDto personaDto = new PersonaDto(998,"manuel", "martin");
    	guardarPersonaDto(personaDto);
    	assertEquals(1, getAllPersonaDtos().size());
        personaDto = getAllPersonaDtos().get(0);         
    	borrarPersona(personaDto);    	
    	assertEquals(null, getPersona(personaDto.getIdentificacion()).getIdentificacion());
    }
    
    @Test
    public void actualizarUnaPersona() throws Exception{
    	final PersonaDto personaDto = new PersonaDto(997,"manuel", "martin");
        guardarPersonaDto(personaDto);
        
        final PersonaDto personaInsertadaDto = getPersona(personaDto.getIdentificacion());
        assertEquals("manuel", personaInsertadaDto.getNombre1());       
                
        personaDto.setNombre1("jose");        
        personaDto.setApellido2("gido");                
        
        actualizarPersonaDto(personaDto);  
        
        assertEquals(personaDto.getNombre1(), getPersona(personaDto.getIdentificacion()).getNombre1());
        assertEquals(personaDto.getApellido2(), getPersona(personaDto.getIdentificacion()).getApellido2());        
    }
    
    @Test
    public void insertarPadreAPersona() throws Exception{
    	final Persona padre = new Persona(888,"Francisco", "martin");
    	final PersonaDto personaDto = new PersonaDto(997,"Esneider", "martin");
    	personaDto.setPadre(padre);
        guardarPersonaDto(personaDto);
        
        final PersonaDto personaInsertadaDto = getPersona(personaDto.getIdentificacion());
        
        assertEquals("Francisco", personaInsertadaDto.getPadre().getNombre1());
    }    
    
    private void actualizarPersonaDto(final PersonaDto personaDto){    	
        testRestTemplate.put("/agenda/persona", personaDto, String.class);
    }
    
    private PersonaDto getPersona(final Integer identificacion){  
    	UriComponentsBuilder builder = UriComponentsBuilder.fromUriString("/agenda/persona")    	        
    	        .queryParam("identificacion", identificacion);
    	
        final ResponseEntity<PersonaDto> personaResponse = testRestTemplate.exchange(builder.build().toUri(),
                HttpMethod.GET, null, PersonaDto.class);
        return personaResponse.getBody();
    }
    
    private void guardarPersonaDto(PersonaDto personaDto){    	
        testRestTemplate.postForEntity("/agenda", personaDto, String.class);
    }
    
    private List<PersonaDto> getAllPersonaDtos() {
        final ResponseEntity<List<PersonaDto>> personaResponse = testRestTemplate.exchange("/agenda",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<PersonaDto>>() {
                });
        return personaResponse.getBody();
    }

    private void borrarPersona(final PersonaDto personaDto){
    	UriComponentsBuilder builder = UriComponentsBuilder.fromUriString("/agenda")    	        
    	        .queryParam("identificacion", personaDto.getIdentificacion());    	
    	
        testRestTemplate.exchange(builder.build().toUri(),
                HttpMethod.DELETE, null, String.class);
    }
    
}
